// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: GPL-3.0-or-later

//! Compute minimum spanning trees (MST) of points in a multi-dimensional space
//! with a defined distance metric.

mod locality_sensitive_hash;
mod nearest_neighbor;

use locality_sensitive_hash::*;
use nearest_neighbor::*;

use std::collections::{BinaryHeap, HashSet, HashMap};
use std::hash::Hash;
use std::cmp::Ordering;
use num_traits::Zero;

pub fn minimum_spanning_tree_hamming_space<P>(points: &[P]) -> HashMap<P, P>
    where P: HammingPoint<Coord=bool> + Clone + Hash + Eq {

    if points.is_empty() {
        return Default::default();
    }

    // Build locality sensitive hash.
    let lsh = {
        let bin_size = 16f64;
        let num_points = points.len() as f64;
        let num_bins = num_points / bin_size;
        let num_sub_samples = num_bins.log2().ceil() as usize;
        let lsh: HammingLSH<_, usize> = HammingLSH::new(num_sub_samples, &points[1..]);
        lsh
    };

    // Build nearest-neigbor search.
    let mut search = LSHNearestNeighbourSearch::new(lsh);

    for p in &points[1..] {
        search.insert(p.clone());
    }


    let mut front = BinaryHeap::new();
    front.push(PQElement {
        distance: 0u32,
        point: points[0].clone(),
        prev: points[0].clone(),
    });

    let mut tree_edges = HashMap::new();

    while let Some(e) = front.pop() {
        if tree_edges.contains_key(&e.point) {
            continue;
        }

        search.remove(&e.point);

        let nearest_neighbor = search.exact_nearest_neighbour(&e.point);

        if let Some(n) = nearest_neighbor {
            // Add a nearest neighbour for the new point.
            let dist = n.distance(&e.point);
            front.push(PQElement {
                distance: dist,
                point: n.clone(),
                prev: e.point.clone(),
            });


            // Add a fresh nearest neighbour for the previous point.
            let nearest_neighbor_prev = search.exact_nearest_neighbour(&e.prev);
            if let Some(n) = nearest_neighbor_prev {
                let dist = n.distance(&e.point);
                front.push(PQElement {
                    distance: dist,
                    point: n.clone(),
                    prev: e.point.clone(),
                });
            }
        }

        // Create edge.
        if e.point != e.prev { // Skip the edge from the first point to itself.
            tree_edges.insert(e.point, e.prev);
        }
    }

    tree_edges
}

struct PQElement<D, V> {
    distance: D,
    point: V,
    prev: V,
}


impl<D: Ord + Eq, V> Ord for PQElement<D, V> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.distance.cmp(&other.distance).reverse()
    }
}

impl<D: PartialOrd, V> PartialOrd for PQElement<D, V> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.distance.partial_cmp(&other.distance).map(|o| o.reverse())
    }
}

impl<D: PartialEq, V> Eq for PQElement<D, V> {}

impl<D: PartialEq, V> PartialEq for PQElement<D, V> {
    fn eq(&self, other: &Self) -> bool {
        self.distance.eq(&other.distance)
    }
}

#[test]
fn test_minmum_spanning_tree() {
    let points: Vec<_> = (0..10).collect();

    let tree_edges = minimum_spanning_tree_hamming_space(&points);

    dbg!(&tree_edges);

    for (a, b) in tree_edges.iter() {
        println!("{}-{}: {}", a, b, a.distance(b));
    }

    assert_eq!(tree_edges.len(), 9);
}
